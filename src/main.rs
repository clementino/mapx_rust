#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate diesel;
extern crate diesel_geometry;
extern crate mapx;
extern crate rocket;

// The URL to the database, set via the `DATABASE_URL` environment variable.
static DATABASE_URL: &'static str = env!("DATABASE_URL");

fn main() {
    rocket::ignite()
        .mount("/", routes![mapx::index])
        .manage(mapx::db::init_pool(DATABASE_URL))
        .launch();
}
