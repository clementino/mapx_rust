use diesel_geometry::pg::data_types::PgPoint;

#[derive(Queryable)]
pub struct Entry {
    pub id: i32,
    pub name: String,
    pub content: String,
    pub location: PgPoint,
}
