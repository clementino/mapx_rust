table! {

    use diesel::sql_types::*;
    use diesel_geometry::pg::types::sql_types::Point;

    entry (id) {
        id -> Int4,
        name -> Varchar,
        content -> Text,
        location -> Point,
    }
}
